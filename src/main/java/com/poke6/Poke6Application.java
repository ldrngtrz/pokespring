package com.poke6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Poke6Application {

	public static void main(String[] args) {
		SpringApplication.run(Poke6Application.class, args);
	}

}
