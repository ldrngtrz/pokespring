package com.poke6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@CrossOrigin
@RestController
public class Poke6Controller {

	@GetMapping("/pokes6")
	public Map<Integer, String> getPokes() {
		System.out.println("poke3 controller : ");
		CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		String result2 = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon?offset=0&limit=300", String.class);
		System.out.println("result2 : " + result2);
		JSONObject root = new JSONObject(result2);
		JSONArray pokes = root.getJSONArray("results");
		List<String> nombres = new ArrayList<String>();
		Map<Integer, String> pokeMapas = new HashMap<Integer, String>();
		for (int i = 0; i < 300; i++) {
			System.out.println("i : " + i + "  name : " + pokes.getJSONObject(i).getString("name"));
			nombres.add(pokes.getJSONObject(i).getString("name"));
			pokeMapas.put(i, pokes.getJSONObject(i).getString("name"));
		}
		System.out.println("pokes : " + pokes.length());
		return pokeMapas;
	}

	@GetMapping("/pokes6/{id}")
	public Map<String,String> getPokesById(@PathVariable Long id) {
		System.out.println("poke3 controller : ");
		CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		String result2 = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon?offset=0&limit=300", String.class);
		System.out.println("result2 : " + result2);
		JSONObject root = new JSONObject(result2);
		JSONArray pokes = root.getJSONArray("results");
		List<String> nombres = new ArrayList<String>();
		Map<String,String> pokeMapas = new HashMap<String,String>();
		for (int i = 0; i < 300; i++) {
			System.out.println("i : " + i + "  name : " + pokes.getJSONObject(i).getString("name"));
			if (i==id) {
			nombres.add(pokes.getJSONObject(i).getString("name"));
			pokeMapas.put("nombre",pokes.getJSONObject(i).getString("name") );
			}
		}
		System.out.println("pokes : " + pokes.length());
		return pokeMapas;
	}

}
